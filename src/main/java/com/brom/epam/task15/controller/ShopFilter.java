package com.brom.epam.task15.controller;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter("/shop/*")
public class ShopFilter implements Filter {
  private final String idPattern = ".*/shop/(\\d).*";
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void destroy() {

  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
      FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
    String path = httpServletRequest.getServletPath();
    try {
      int id = getId(httpServletRequest.getRequestURI());
      httpServletRequest.setAttribute("id", id);
    } catch (NoIdException e) {

    }
    filterChain.doFilter(httpServletRequest, servletResponse);
  }

  private int getId(String URI) {
    Pattern p = Pattern.compile(idPattern);
    Matcher m = p.matcher(URI);
    if (m.matches()) {
      System.out.println(m.group(0));
      System.out.println(m.group(1));
      return Integer.parseInt(m.group(1));
    }
    throw new NoIdException();
  }

  private class NoIdException extends RuntimeException {

  }
}
