package com.brom.epam.task15.repository;

import java.util.ArrayList;
import java.util.List;
import com.brom.epam.task15.model.Shop;

public class ShopRepository {
  private static ShopRepository shopRepository = new ShopRepository();
  private List<Shop> shops;
  private ShopRepository() {
    this.shops = new ArrayList<>();
    initShops();
  }

  public static ShopRepository getInstance() {
    return shopRepository;
  }
  private void initShops() {
    Shop shop = new Shop("All fo you", "Lviv, S.Bandery 12");
    shops.add(shop);
    Shop shop2 = new Shop("All fo you", "Lviv, S.Bandery 12");
    shops.add(shop2);
    Shop shop3 = new Shop("All fo you", "Lviv, S.Bandery 12");
    shops.add(shop3);
    Shop shop4 = new Shop("All fo you", "Lviv, S.Bandery 12");
    shops.add(shop4);
  }

  public Shop getShopById(int id) {
    return shops.get(id);
  }

  public List<Shop> getShops() {
    return shops;
  }

  public void addShop(String name, String address) {
    Shop shop = new Shop(name, address);
    this.shops.add(shop);
  }

  public void removeShop(int id) {
    this.shops.remove(id);
  }
}
