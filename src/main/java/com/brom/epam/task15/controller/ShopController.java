package com.brom.epam.task15.controller;

import com.brom.epam.task15.model.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.brom.epam.task15.model.Shop;
import com.brom.epam.task15.repository.ShopRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet(name = "shopServlet", urlPatterns = "/shop/*")
public class ShopController extends HttpServlet {
  private Logger logger = LogManager.getLogger(ShopController.class);
  ShopRepository shopRepository = ShopRepository.getInstance();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    PrintWriter printWriter = resp.getWriter();
    if (req.getAttribute("id") != null) {
      int id = (Integer)req.getAttribute("id");
      Shop shop = shopRepository.getShopById(id);
      List<Product> products = shop.getProducts();
      printWriter.write("<table>");
      for (int i = 0; i < products.size(); i++) {
        Product product = products.get(i);

        printWriter.write("<tr>");
        printWriter.write("<td><a href = '"+ req.getRequestURI()+"/" + i + "'>" + product.getName() +  "</a></td>");
        printWriter.write("<td>" + product.getPrice() +  "</td>");
//        printWriter.write("<td onclick = 'var request = new XMLHttpRequest();\n"
//            + "request.open(\"DELETE\",\"" + req.getRequestURI() + "/" + i +"/delete\", false);\n"
//            + " request.send(); window.location = \""
//            + req.getRequestURI().replaceAll("/\\d+/delete", "")
//            +"\"'>DELETE</td>");
        printWriter.write("</tr>");
      }
      printWriter.write("</table>");
    } else {
        List<Shop> shops = shopRepository.getShops();
        printWriter.write("<table>");
        for (int i = 0; i < shops.size(); i++) {
          Shop shop = shops.get(i);
          printWriter.write("<tr>");
          printWriter.write("<td><a href = '"+ req.getRequestURI()+"/" + i + "'>" + shop.getName() +  "</a></td>");
          printWriter.write("<td>" + shop.getAddress() +  "</td>");
          printWriter.write("<td onclick = 'var request = new XMLHttpRequest();\n"
              + "request.open(\"DELETE\",\"" + req.getRequestURI() + "/" + i +"/delete\", false);\n"
              + " request.send(); window.location = \""
              + req.getRequestURI().replaceAll("/\\d+/delete", "")
              +"\"'>DELETE</td>");
          printWriter.write("</tr>");
        }
        printWriter.write("</table>");
        printWriter.write("<form method='POST' action='" + req.getRequestURI() +"/add-shop'>");
        printWriter.write("<label>name</label><input name = 'name'>");
        printWriter.write("<label>address</label><input name = 'address'>");
        printWriter.write("<input type = 'submit'>");
        printWriter.write("</form>");
    }
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    logger.info("delete");
    int id = (Integer)req.getAttribute("id");
    shopRepository.removeShop(id);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    logger.info("post");
    String name = req.getParameter("name");
    String address = req.getParameter("address");
    shopRepository.addShop(name, address);
    resp.sendRedirect(req.getRequestURL().toString().replace("/add-shop", ""));
  }
}
