package com.brom.epam.task15.model;

import java.util.ArrayList;
import java.util.List;

public class Shop {
  private String name;
  private String address;
  private List<Product> products;

  public Shop(String name, String address) {
    this.name = name;
    this.address = address;
    this.products = new ArrayList<>();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void addProduct(Product product) {
    this.products.add(product);
  }

  public List<Product> getProducts() {
    return products;
  }

  @Override
  public String toString() {
    return "Shop{" +
        "name='" + name + '\'' +
        ", address='" + address + '\'' +
        '}';
  }
}
